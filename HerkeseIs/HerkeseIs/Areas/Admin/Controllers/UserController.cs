﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HerkeseIs.Areas.Admin.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
       
        is_vbEntities db = new is_vbEntities();
        // GET: Admin/User
        public ActionResult Kisiler()
        {
            var model = db.Users.OrderByDescending(x => x.ID).ToList();
            return View(model);
        }
        public ActionResult Add()
        {
            return View();
        }
        public ActionResult AddUser(Users model)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(model);
                db.SaveChanges();
            }
            return RedirectToAction("Kisiler");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = db.Users.Find(id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(Users model)
        {
            if (ModelState.IsValid)
            {
                db.Users.Attach(model);
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Kisiler");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var model = db.Users.Find(id);
            return View(model);
        }
        [HttpGet]
        public ActionResult DeleteConfirm(int id)
        {
            var model = db.Users.Find(id);
            db.Users.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Kisiler");
        }
    }
}