﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HerkeseIs.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using HerkeseIs.Areas.Admin.Models;

namespace HerkeseIs.Areas.Admin.Controllers
{
    public class RoleController : Controller
    {
        ApplicationDbContext context = new ApplicationDbContext();
        // GET: Admin/Role
        public ActionResult Index()
        {
            var RoleStore = new RoleStore<IdentityRole>(context);
            var RoleManager = new RoleManager<IdentityRole>(RoleStore);
            var model = RoleManager.Roles.ToList();


            return View(model);
        }

        public ActionResult RoleEkle()
        {


            return View();
        }
        [HttpPost]
        public ActionResult RoleEkle(RoleEkleModelcs rol)
        {

            var RoleStore = new RoleStore<IdentityRole>(context);
            var RoleManager = new RoleManager<IdentityRole>(RoleStore);
           if( RoleManager.RoleExists(rol.RolAd) == false)
            {
                RoleManager.Create(new IdentityRole(rol.RolAd));
            }
            return RedirectToAction("Index");
        }

        public ActionResult RoleUserEkle()
        {
            return View();
        }
    }
}