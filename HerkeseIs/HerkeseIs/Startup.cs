﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HerkeseIs.Startup))]
namespace HerkeseIs
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
